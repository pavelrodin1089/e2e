import os

from dopplersdk import DopplerSDK
from dotenv import load_dotenv

load_dotenv()

doppler = DopplerSDK(access_token=os.getenv('DOPPLER_TOKEN'))
