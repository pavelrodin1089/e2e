import base64
import json
import requests
import pybase64
import os

from helpers.client import Client


def send_report(user: Client, path_to_allure_results_dir: str):
    # user.abilities_to.send("POST", "http://localhost:5050/allure-docker-service/projects", {"id": "e2e"},
    #                        headers={"Content-Type": "application/json"})
    # user.abilities_to.send("GET", "http://localhost:5050/allure-docker-service/clean-results?project_id=e2e", data={})
    print(len([entry for entry in os.scandir(path_to_allure_results_dir)]))
    files = os.listdir(path_to_allure_results_dir)
    print(f"This is files {len(files)}")

    results = []
    for file in files:
        result = {}

        file_path = path_to_allure_results_dir + "/" + file
        print(file_path)
        # print(f"This is file name {file}")
        # print(f"This is file type {type(file)}")
        # if os.path.isfile(file_path):
        try:
            with open(file_path, "rb") as f:
                content = f.read()
                if content.strip():
                    b64_content = base64.b64encode(content)
                    result['file_name'] = file
                    result['content_base64'] = b64_content.decode('UTF-8')
                    results.append(result)
                else:
                    print('Empty File skipped: ' + file)
        finally:
            f.close()
        # else:
        #     print('Directory skipped: ' + file)
    request_body = {
        "results": results
    }
    json_request_body = json.dumps(request_body)
    print(f"This is json {json_request_body}")
    print("------------------SEND-RESULTS------------------")

    response = user.abilities_to.send("POST", "http://localhost:5050/allure-docker-service/send-results?project_id=e2e",
                                      data=json_request_body, headers={"Content-Type": "application/json"})
    # response = requests.post("http://localhost:5050/allure-docker-service/send-results?project_id=e2e", headers={'Content-type': 'application/json'},
    #               data=json_request_body)
    # print("STATUS CODE:")
    # print(response.status_code)
    # print("RESPONSE:")
    # json_response_body = json.loads(response.content)
    # json_prettier_response_body = json.dumps(json_response_body, indent=4, sort_keys=True)
    # print(json_prettier_response_body)

    # with open(file, "rb") as f:
    #     encoded_string = pybase64.b64encode(f.read())
    #     print(f"{encoded_string=}")
    #     # result["file_name"] = f.name
    #     # result["content_base64"] = encoded_string.decode("UTF-8")
    #     # results.append(result)
    #     # requests.post()
    #     user.abilities_to.send("POST", "http://localhost:5050/allure-docker-service/send-results?project_id=e2e",
    #                            data={
    #                                "results": [{
    #                                    "file_name": file.name,
    #                                    # "content_base64": encoded_string
    #                                    "content_base64": encoded_string.decode("UTF-8")
    #                                }]
    #                            })

    print("------------------GENERATE-REPORT------------------")
    execution_name = 'execution from my script'
    execution_from = 'http://google.com'
    execution_type = 'teamcity'
    # response = requests.get(
    #     allure_server + '/allure-docker-service/generate-report?project_id=' + project_id + '&execution_name=' + execution_name + '&execution_from=' + execution_from + '&execution_type=' + execution_type,
    #     headers=headers, verify=ssl_verification)
    user.abilities_to.send("GET",
                           f"http://localhost:5050/allure-docker-service/generate-report?project_id=e2e&execution_name={execution_name}&execution_from={execution_from}&execution_type={execution_type}",
                           data={1: "fill_data"})
    # print("STATUS CODE:")
    # print(response)
    # print("RESPONSE:")
    # json_response_body = json.loads(response.content)
    # json_prettier_response_body = json.dumps(json_response_body, indent=4, sort_keys=True)
    # print(json_prettier_response_body)

    # user.abilities_to.send("POST", "http://localhost:5050/allure-docker-service/send-results?project_id=e2e",
    #                        data={"results": results})
    # user.abilities_to.send("GET", "http://localhost:5050/allure-docker-service/generate-report?project_id=e2e&execution_name=autotest")
    # user.abilities_to.send("GET", "http://localhost:5050/allure-docker-service/generate-report?project_id=default&execution_name=Allure%20Docker%20Service%20UI", data={})
    # requests.get("http://localhost:5050/allure-docker-service/generate-report?project_id=default&execution_name=autotest")
