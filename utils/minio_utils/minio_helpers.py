import json
from minio import Minio
from minio.helpers import ObjectWriteResult

from utils.doppler_utils.doppler_helpers import doppler


class MinioHelpers:
    client = Minio("127.0.0.1:9000",
                   access_key=doppler.secrets.get("MINIO_ACCESS_KEY", "dev", "e2e-tests").value["raw"],
                   secret_key=doppler.secrets.get("MINIO_SECRET_KEY", "dev", "e2e-tests").value["raw"],
                   secure=False,
                   )

    @staticmethod
    def upload_to_minio(source_file_path: str, bucket_name: str, object_name: str) -> ObjectWriteResult:
        found = MinioHelpers.client.bucket_exists(bucket_name)
        if not found:
            print(f"Creating bucket {bucket_name}")
            MinioHelpers.client.make_bucket(bucket_name)
        else:
            print("Bucket", bucket_name, "already exists")
        policy = {"Version": "2012-10-17",
                  "Statement": [{"Effect": "Allow", "Principal": {"AWS": ["*"]}, "Action": "*",
                                 "Resource": [f"arn:aws:s3:::{bucket_name}/*"]}]}
        MinioHelpers.client.set_bucket_policy(bucket_name, policy=json.dumps(policy))

        return MinioHelpers.client.fput_object(
            bucket_name=bucket_name, object_name=object_name, file_path=source_file_path, content_type="application/zip"
        )
