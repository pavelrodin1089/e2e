
def add_token(func):
    def wrapper(self, *args, **kwargs):
        headers = kwargs.get("headers", False)
        if headers:
            headers["Authorization"] = f"Bearer {self.client_instance.token}"
        else:
            headers = {"Authorization": f"Bearer {self.client_instance.token}"}
        kwargs["headers"] = headers
        return func(self, *args, **kwargs)
    return wrapper
