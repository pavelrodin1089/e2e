from helpers.api_client import ClientApi
from typing import Optional


class Client:
    def __init__(self, user_name: Optional[str], password: Optional[str],
                 url: Optional[str], **kwargs):
        self.user_name = user_name if user_name else None
        self.password = password if password else None
        self.token = kwargs.get("token") if kwargs.get("token", False) else ""
        self.id = kwargs.get("id") if kwargs.get("id", False) else None
        self.url = url if url else None
        self.abilities_to = kwargs.get("abilities", False) if kwargs.get("abilities", False) else ClientApi(self)
        self.headers = kwargs.get("headers") if kwargs.get("headers", False) else {"Content-Type": "application/json"}
        self.api_skills = ClientApi(self)
