import json

import allure
import requests
from requests import Request, Session

from utils.function_to_help import add_token


class ClientApi:
    def __init__(self, client_instance):
        self.session = Session()
        self.client_instance = client_instance

    @add_token
    def send(self, method: str, uri: str, data: dict, headers: dict = {"Content-Type": "application/json"}):
        req = Request(method=method, url=uri, data=data, headers=headers).prepare()
        r = self.session.send(req)
        if r.status_code == 401:
            self.client_instance.abilities_to.get_token("POST", f"{self.client_instance.url}Account/v1/GenerateToken",
                                                        {"userName": self.client_instance.user_name,
                                                         "password": self.client_instance.password})
            headers["Authorization"] = f"Bearer {self.client_instance.token}"
            r = self.session.send(Request(method=method, url=uri, json=data, headers=headers).prepare())
        allure.attach(req.body, f"{method} to {uri}", attachment_type=allure.attachment_type.JSON)
        if r.status_code != 204:
            allure.attach(r.text, f"Response {r.status_code}", attachment_type=allure.attachment_type.JSON)
            return r.json()
        else:
            allure.attach("{}", f"Response {r.status_code}", attachment_type=allure.attachment_type.TEXT)
            return {}

    def get_token(self, method: str, uri: str, data: dict, headers: dict = {"Content-Type": "application/json"}):
        req = Request(method=method, url=uri, json=data, headers=headers).prepare()
        r = self.session.send(req)
        self.client_instance.token = r.json()["token"]
        allure.attach(req.body, f"{method} to {uri}", attachment_type=allure.attachment_type.JSON)
        allure.attach(r.text, f"Response {r.status_code}", attachment_type=allure.attachment_type.JSON)
