import base64
import json
import os

import requests


def send_resul():
    current_path = os.getcwd() + "/allure-results"
    files = os.listdir(current_path)

    results = []
    for file in files:
        result = {}

        file_path = current_path + "/" + file
        try:
            with open(file_path, "rb") as f:
                content = f.read()
                if content.strip():
                    b64_content = base64.b64encode(content)
                    result['file_name'] = file
                    result['content_base64'] = b64_content.decode('UTF-8')
                    results.append(result)
                else:
                    print('Empty File skipped: ' + file)
        finally:
            f.close()
    request_body = {
        "results": results
    }
    json_request_body = json.dumps(request_body)
    print("------------------SEND-RESULTS------------------")
    e2e_resolution = requests.post(url="http://localhost:5050/allure-docker-service/projects", json={"id": "e2e"},
                           headers={"Content-Type": "application/json"})
    print("Creating project e2e")
    print(f"{e2e_resolution.content=}")
    results_send_info = requests.post("http://localhost:5050/allure-docker-service/send-results?project_id=e2e",
                  headers={'Content-type': 'application/json'}, data=json_request_body)
    print("Results sending status")
    print(f"{results_send_info.content=}")
    print("------------------GENERATE-REPORT------------------")
    execution_name = 'execution from my script'
    execution_from = 'http://google.com'
    execution_type = 'teamcity'
    report_gen_info = requests.get(
        f"http://localhost:5050/allure-docker-service/generate-report?project_id=e2e&execution_name={execution_name}&execution_from={execution_from}&execution_type={execution_type}",
        headers={'Content-type': 'application/json'})
    print("Report generation status")
    print(f"{report_gen_info.content=}")


if __name__ == "__main__":
    send_resul()
