from playwright.sync_api import Page, Locator


class BookStore:
    def __init__(self, page: Page):
        self.page = page
        self.app_main_page = page.locator("//*[descendant::*[text()='Book Store Application'] and @class='card-body']")
        self.book_shelf = page.locator("//*[@class='rt-tbody']")

    def __book_row_by_some_text__(self, text: str):
        return self.page.locator(f"//*[child::div[contains(text(), '{text}')]]")

    def __locator_by_id__(self, id: str) -> Locator:
        return self.page.locator(f"//*[@id='{id}']")

    def by_id(self, id: str) -> Locator:
        return self.__locator_by_id__(id=id)

    def delete_book_from_shelf(self, text: str):
        return self.__book_row_by_some_text__(text).locator("//*[@title='Delete']")

    @property
    def captcha(self) -> Locator:
        return self.__locator_by_id__("recaptcha-anchor")

    @property
    def close_modal_window(self) -> Locator:
        return self.__locator_by_id__("closeSmallModal-ok")

    @property
    def first_name_input(self) -> Locator:
        return self.__locator_by_id__("firstname")

    @property
    def lastname_input(self) -> Locator:
        return self.__locator_by_id__("lastname")

    @property
    def new_user_btn(self) -> Locator:
        return self.__locator_by_id__("newUser")

    @property
    def password(self) -> Locator:
        return self.__locator_by_id__("password")

    @property
    def register_btn(self) -> Locator:
        return self.__locator_by_id__("register")

    @property
    def start_login(self) -> Locator:
        return self.__locator_by_id__("login")

    @property
    def user_name_input(self) -> Locator:
        return self.__locator_by_id__("userName")

    @property
    def user_name_value(self) -> Locator:
        return self.__locator_by_id__("userName-value")
