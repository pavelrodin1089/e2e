import re
import faker
import allure
import pytest
from playwright.sync_api import Page, expect
from po.book_store import BookStore


@allure.title("Проверка сайта на корректное открытие")
def test_has_title(page: Page, allure_step):
    with allure_step("Переход на главную страницу"):
        page.goto("")
    with allure_step("Проверка title страницы"):
        expect(page).to_have_title(re.compile("DEMOQA"))


@allure.title("Невалидный пользователь")
def test_wrong_login(page: Page, allure_step):
    book_store = BookStore(page=page)
    with allure_step("Переход на главную страницу"):
        page.goto("")
    with allure_step("Переход на страницу книжного магазина"):
        book_store.app_main_page.click()
    with allure_step("Открытие страницы ввода логина и пароля"):
        book_store.by_id("login").click()
    with allure_step("Ввод невалидных логина и пароля"):
        book_store.by_id("userName").fill("asf")
        book_store.by_id("password").fill("asf")
    with allure_step("Попытка залогиниться"):
        book_store.by_id("login").click()
    with allure_step("Наличие сообщения о неправильных данных"):
        expect(book_store.by_id("name")).to_have_text("Invalid username or password!")


@allure.title("Создание пользователя")
def test_create_user(page: Page, allure_step, faker: faker.Faker):
    book_store = BookStore(page=page)
    with allure_step("Переход на главную страницу"):
        page.goto("")
    with allure_step("Переход на страницу книжного магазина"):
        book_store.app_main_page.click()
    with allure_step("Открытие страницы ввода логина и пароля"):
        book_store.by_id("login").click()
    with allure_step("Переход на страницу создания пользователя"):
        book_store.new_user_btn.click()
    with allure_step("Заполнение полей пользователя"):
        book_store.first_name_input.fill(faker.first_name())
        book_store.lastname_input.fill(faker.last_name())
        book_store.user_name_input.fill("".join(faker.random_letters(10)))
        book_store.password.fill(faker.password())
        frames = page.frames
        print("Frames", frames)
        locator = page.frame_locator(f"iframe[name=\"{frames[-1].name}\"]").get_by_text("I'm not a robot")

        locator.click()
        # book_store.captcha.check(force=True)
        page.get_by_label("I'm not a robot").dblclick()
        # page.get_by_role('checkbox').check()
    with allure_step("Регистрация пользователя"):
        book_store.register_btn.click()


@allure.title("Логин в систему")
def test_login(page: Page, allure_step, new_user):
    book_store = BookStore(page=page)
    with allure_step("Переход на главную страницу"):
        page.goto("")
    with allure_step("Переход на страницу книжного магазина"):
        book_store.app_main_page.click()
    with allure_step("Открытие страницы ввода логина и пароля"):
        book_store.start_login.click()
    with allure_step("Заполнение формы"):
        book_store.user_name_input.fill(new_user.user_name)
        book_store.password.fill(new_user.password)
    with allure_step("Нажатие на кнопку Login"):
        book_store.start_login.click()
    with allure_step("Успешно залогинились"):
        expect(book_store.user_name_value).to_have_text(new_user.user_name)


@allure.title("Удаление книги из корзины")
def test_delete_book_from_shelf(page: Page, allure_step, new_user):
    book_store = BookStore(page=page)
    with allure.step("Добавляем книги в корзину"):
        books_in_store = new_user.abilities_to.send("GET", f"{new_user.url}BookStore/v1/Books", {})
        first_book_info = books_in_store['books'][0]
        second_book_info = books_in_store['books'][1]
        new_user.abilities_to.send("POST", f"{new_user.url}BookStore/v1/Books", {
            "userId": f"{new_user.id}",
            "collectionOfIsbns": [{
                "isbn": f"{first_book_info['isbn']}"
            },
                {
                    "isbn": f"{second_book_info['isbn']}"
                }]
        })
    with allure_step("Переход на главную страницу"):
        page.goto("login")
    with allure_step("Заполнение формы"):
        book_store.user_name_input.fill(new_user.user_name)
        book_store.password.fill(new_user.password)
    with allure_step("Нажатие на кнопку Login"):
        book_store.start_login.click()
    with allure_step("В корзине две книги"):
        expect(book_store.book_shelf).to_contain_text(first_book_info['title'])
        expect(book_store.book_shelf).to_contain_text(second_book_info['title'])
    with allure_step("Удаляем первую из них"):
        book_store.delete_book_from_shelf('Richard E. Silverman').click()
        book_store.close_modal_window.click()
    with allure_step("В корзине одна книга"):
        expect(book_store.book_shelf).not_to_contain_text(first_book_info['title'])
        expect(book_store.book_shelf).to_contain_text(second_book_info['title'])
