from datetime import timedelta

import pytest
import allure
import glamor
import os
from contextlib import contextmanager
import faker
from random import randint

from utils.minio_utils.minio_helpers import MinioHelpers
from helpers.client import Client

glamor.include_scope_in_title('after', autouse=True)


@glamor.title.setup(hidden=True)
@pytest.fixture
def allure_step(page):
    @contextmanager
    def allure_step_context(title):
        with allure.step(title) as step:
            try:
                yield step
            finally:
                screenshot = page.screenshot(full_page=True)
                allure.attach(
                    screenshot,
                    name="screenshot.png",
                    attachment_type=allure.attachment_type.PNG
                )

    return allure_step_context


@glamor.title.setup(hidden=True)
@pytest.fixture(scope="function", autouse=True)
def attach_allure_trace(request: pytest.FixtureRequest, pytestconfig: pytest.Config):
    yield

    start_point = pytestconfig.invocation_params.dir
    test_file_name = os.path.split(request.path)[1]
    output_dir = pytestconfig.getoption("--output")
    test_name = request.node.name

    char_to_replace = ["_", ".", "["]
    for char in char_to_replace:
        test_file_name = test_file_name.replace(char, "-")
        test_name = test_name.replace(char, "-")
    test_name = test_name[:-1]

    path_to_zip_file = f"{start_point}/{output_dir}/e2e-tests-{test_file_name}-{test_name}/trace.zip"
    uploaded_object = MinioHelpers.upload_to_minio(source_file_path=path_to_zip_file,
                                                   bucket_name=test_name,
                                                   object_name="trace.zip")

    url = MinioHelpers.client.presigned_get_object(
        bucket_name=test_name,
        object_name="trace.zip",
        expires=timedelta(hours=2),
        version_id=uploaded_object.version_id
    )

    allure.dynamic.description_html(f'<a href={url} download> Download trace </a>')
    allure.dynamic.link(f'https://trace.playwright.dev/?trace={url}', name="Tool to watch trace")

    if True:
        glamor.dynamic.title.teardown(hidden=True)


@pytest.fixture(autouse=True)
def faker_seed():
    return randint(0, 100000)


@glamor.title.setup("Создание нового пользователя")
@glamor.title.teardown('Удаление созданного пользователя')
@pytest.fixture(scope="session")
def new_user(base_url, _session_faker: faker.Faker):
    user_name = _session_faker.profile(fields=['username'])['username']
    password = _session_faker.password(length=randint(10, 20))
    new_user = Client(user_name=user_name, password=password, url=base_url)
    response_creation = new_user.abilities_to.send("POST", f"{base_url}Account/v1/User",
                                                   {"userName": user_name, "password": password})

    new_user.id = response_creation['userID']
    new_user.abilities_to.get_token("POST", f"{base_url}Account/v1/GenerateToken",
                                    {"userName": new_user.user_name, "password": new_user.password})

    yield new_user

    new_user.abilities_to.send("DELETE", f"{base_url}Account/v1/User/{new_user.id}", data={1: "fill_data"})
